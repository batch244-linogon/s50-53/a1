// import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Courses from './pages/Courses';
import Logout from './pages/Logout'
import './App.css';


function App() {
  return(
    // In React JS, multiple components rendered in a single component should be wrapped in a parent components
    // "Fragment" ensures that an error will be prevented
    <Router>
        <AppNavbar/>
        {/*<Home/>*/}
        <Container>
          <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/courses" element={<Courses/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/register" element={<Register/>} /> 
          <Route path="/logout" element={<Logout/>} /> 
          </Routes>
        </Container>
          
    </Router>
  );
}

export default App;