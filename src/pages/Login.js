import {Form, Button} from 'react-bootstrap';
import {useState,useEffect} from 'react';

export default function Login(){

	// State hooks to store the values of the input fields 
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	function regUser(e) {

		// Prevents the page reloading
		e.preventDefault();

		// Set the email of the authenticated user in the local storage
		// Syntax: localStorage.setItem('propertyName', value)
		localStorage.setItem('email', email);

		// Clear input fields
		setEmail(''); 
		setPassword1('');

		alert('You are now logged in.');
	}


	useEffect(() => {
		if(email !== '' && password1 !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email,password1]);

	  function refreshLogin(){
    window.location.reload();
  }

	return (
		// Invoke the register user function upon clicking on submit button
		<Form onSubmit={(e) => regUser(e)}>

			<Form.Label className="fs-1 fw-bold"> Login </Form.Label>

			<Form.Group controlId = "userEmail">
				<Form.Label> Email Address </Form.Label>
				<Form.Control
					type="email"
					placeholder = "Enter email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted"> We'll never share your email with anyone else. </Form.Text>
			</Form.Group>

			<Form.Group className="pb-3" controlId = "password1">
				<Form.Label> Password </Form.Label>
				<Form.Control
					type="password"
					placeholder = "Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
				
			</Form.Group>

			

		{/*conditional render submit button based on isActive state*/}

		{/* */}

		{isActive
			? 
				<Button variant="success" type="submit" id="submitBtn" onClick={refreshLogin}> Submit </Button>
			:
				<Button variant="danger" type="submit" id="submitBtn" disabled> </Button>
		}
		

		</Form>

		)

}