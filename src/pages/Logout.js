import {Navigate } from 'react-router-dom';

export default function Logout(){

	/*
	"localStorage.clear()" is a method that allows us to clear the information in the localStorage ensuring the no information is stored in our browser
	*/

	localStorage.clear()


	return (
			<Navigate to="/login" />

		)
}